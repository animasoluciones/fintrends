<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package start
 */

get_header(); ?>

	<div id="primary" class="content-area pd-0">
		<main id="main" class="site-main col-md-12 pd-0" role="main">

            <?php if ( have_posts() ) : ?>
                <?php get_template_part( 'template-parts/content', 'featured' ); ?>
                <?php get_template_part( 'template-parts/content', 'opinions' ); ?>
                <?php get_template_part( 'template-parts/content', 'international' ); ?>
                <?php get_template_part( 'template-parts/content', 'events' ); ?>

            <?php else : ?>

                <?php get_template_part( 'template-parts/content', 'none' ); ?>

            <?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<!--?php get_sidebar(); ?-->
<?php get_footer(); ?>
