<?php
/**
 * Created by PhpStorm.
 * User: yus
 * Date: 19/10/17
 * Time: 13:43
 */
?>
<?php $query = new WP_Query( array( 'cat' => 36 ) ); $i=0; ?>
<section class="flex-container flags mt-5 <?php echo $query->have_posts()==true ?:'oculto';?>">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h2 class="uppercase">Opinión</h2>
    </div>
</section>
<section class="flex-container mt-5 relative">
    <div class="display-flex opinion">
        <?php while ( $query->have_posts() and $i<=6) : $query->the_post(); ?>
        <div data-link="<?php the_permalink(); ?>" class="new-tab opinion-items <?php echo ($i>=4 ? 'oculto':'mostrar '); echo ($i%3==0 ?' pr-0 pl-2 ':' pr-0 pl-2 '); echo ($i==0?' f-0':'')?>">
            <div class="opinion-pic" style="background-image:url( <?php echo wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' )[0];;?>);background-repeat: no-repeat;background-size: cover;">
            </div>
            <div class="opinion-title">
                <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                <?php foreach (wp_get_post_tags( get_the_ID()) as $index => $value){?>
                    <a href="#"><span><?php echo $value->name?></span></a>
                <?php } ?>
            </div>
        </div>

            <?php $i++;?>

        <?php endwhile; ?>
    </div>
    <span class="prev"></span>
    <span class="next"></span>
</section>


