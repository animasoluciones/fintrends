<?php
/**
 * @package start
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header single-entry-header">
		<div class="title-header">
            <?php the_title( '<h1 class="entry-title title-entry-single ">', '</h1>' ); ?>

            <div class="entry-meta">
                <p class=""><?php echo get_the_author();?>  <?php the_date('d/m/Y', '<span>', '</span>'); ?></p>
            </div><!-- .entry-meta -->
        </div>

		<div class="img-header-single">
            <?php the_post_thumbnail(); ?>
        </div>
	</header><!-- .entry-header -->

	<div class="container">
        <div class="entry-content">
            <?php the_content(); ?>
            <!--?php
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'start' ),
                    'after'  => '</div>',
                ) );
            ?-->
        </div><!-- .entry-content -->
        <footer class="entry-footer">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="foto float-letf mt-2">
                    <?php printf( get_avatar( get_the_author_meta('ID'), 64, '', '', array('class'=>'img-autor img-responsive') )); ?>
                </div>
                <div class="foto float-letf mt-2">
                    <ul class="mg-0 autor-post">
                        <li><p class="mg-0 uppercase"><?php echo get_the_author();?></p></li>
                        <li><a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/twitter.png" width="25"> </a> <a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/linkedin.png" width="25"> </a> </li>
                    </ul>
                </div>
                <div class="share-icon">
                    <p>Compartir este articulo en</p>
                    <ul class="mg-0 autor-post">
                        <li><a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/twitter.png" width="25"> </a> <a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/linkedin.png" width="25"> </a> </li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 news-r">
                <h1>Noticias relacionadas</h1>
                <?php $query = new WP_Query( array( 'cat' => get_the_category()[0]->term_id ) ); $i=1; ?>
                <section class="flex-container section-featured">
                    <?php while ( $query->have_posts() and $i<=4) : $query->the_post(); ?>
                        <?php if($i>1):?>
                            <div class="flex-item-4 card card-footer">
                                <!--img src="img_avatar.png" alt="Avatar" style="width:100%"-->
                                <div class="pic-post float-letf">
                                    <?php echo get_the_post_thumbnail( get_the_ID(),'full' );?>
                                    <div class="tittle-card float-letf">
                                        <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                                    </div>
                                </div>

                            </div>
                        <?php endif;?>
                        <?php $i++;?>

                    <?php endwhile; ?>

                    <!--?php start_the_posts_navigation(); ?-->
                </section>
            </div>
        </footer><!-- .entry-footer -->
    </div>
</article><!-- #post-## -->
