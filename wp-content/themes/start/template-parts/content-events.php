<?php
/**
 * User: yus
 * Date: 19/10/17
 * Time: 13:43
 */
?>
<?php $query = new WP_Query(
                                array(
                                        'post_type' => 'tribe_events',
                                         'order'   => 'DESC',
                                    )
                            );
        $i=1;
?>

<?php $videos = new WP_Query(
    array(
        'cat' => 46,
    )
);
$j=1;

?>

<section class="flex-container mt-5 <?php echo $query->have_posts() == true ?: 'oculto';?>"
         xmlns="http://www.w3.org/1999/html">
    <div class="col-xs-12 col-sm-12 flags col-md-12">
        <h2 class="uppercase">Eventos y formación</h2>
    </div>
        <div class="col-xs-12 col-sm-8 col-md-8 mt-2 pd-0">
                <div class="col-xs-12 col-sm-6 col-md-6 calendario">
                    <img class="img-responsive img-calen" src="<?php echo get_theme_root_uri()?>/start/img/calendar.png" width="40">
                    <div class="flechas">
                        <span class="sig"></span>
                        <p class="meses uppercase" data-mes="<?php echo date('m');?>"><?php echo date_i18n('F');  ?></p>
                        <span class="ant"></span>
                    </div>
                    <table class="table tab-eventos">
                        <thead>
                        <tr>
                            <th>Dom</th>
                            <th>Lun</th>
                            <th>Mar</th>
                            <th>Mie</th>
                            <th>Jue</th>
                            <th>Vie</th>
                            <th>Sab</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>

                    </table>
                </div>
                <?php while ( $query->have_posts() and $i<=3) : $query->the_post(); ?>
                    <div class="col-md-12 pd-0 mt-6 box-1">
                        <!--img src="img_avatar.png" alt="Avatar" style="width:100%"-->
                        <div class="float-letf col-md-1 pd-0 e-date">
                            <p class="text-center mb-1 p-dias"><?php
                                if(get_post_meta(get_the_ID(),'dias',true)){
                                    printf (get_post_meta(get_the_ID(),'dias',true).'<br/><span class="span-m">'.date('M',strtotime(get_post_meta(get_the_ID(),'_EventStartDateUTC',true))).'</span>');
                                }else{
                                    printf( date('d',strtotime(get_post_meta(get_the_ID(),'_EventStartDateUTC',true)))
                                        .'-'.date('d',strtotime(get_post_meta(get_the_ID(),'_EventEndDateUTC',true)))
                                        .'<br/><span class="span-m">'.date('M',strtotime(get_post_meta(get_the_ID(),'_EventStartDateUTC',true))).'</span>');
                                }

                                ?>
                            </p>

                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 pl-0 float-letf event-img">
                            <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( get_the_ID(),'full' );?></a>
                        </div>
                        <div class="float-letf col-xs-12 col-sm-5 col-md-5 pr-0 e-cont ">
                            <a href="<?php the_permalink(); ?>"><h4 class="mg-0"><?php the_title(); ?></h4></a>
                            <?php echo  the_excerpt();?>
                        </div>
                    </div>
                    <?php $i++;?>

                <?php endwhile; ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 pd-0">
            <div class="videos pd-0">
                <?php while ( $videos->have_posts() and $j<=3) : $videos->the_post(); ?>
                    <div class="videos-items pd-0 <?php echo ($j>=4 ? 'oculto':'mostrar')?>">
                        <div class="opinion-title">
                            <a href="<?php the_permalink(); ?>"><h2 class="text-center"><?php the_title(); ?></h2></a>
                        </div>
                        <div class="frame">
                            <?php echo the_content();?>
                        </div>
                        <div class="category-name mt-1">
                            <h4 class="mg-0 uppercase"><?php echo  get_the_category()[0]->name;?></h4>
                        </div>
                    </div>

                    <?php $j++;?>

                <?php endwhile; ?>
            </div>
        </div>
</section>