<?php
/**
 * User: yus
 * Date: 18/10/17
 * Time: 12:23
 */
?>
<?php $query = new WP_Query( array( 'cat' => 4 ) ); $i=1; ?>
<section class="flex-container flags mb-4 <?php echo $query->have_posts()==true ?:'oculto';?>">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h2 class="uppercase">Lo último</h2>
    </div>
</section>
<section class="flex-container section-featured <?php echo $query->have_posts()==true ?:'oculto';?>">
       <?php while ( $query->have_posts() and $i<=6) : $query->the_post(); ?>
        <div class="flex-item-4 card <?php echo ($i==2 ? ' card-padding':''); echo ($i==5 ? ' card-padding':''); ?>">
            <!--img src="img_avatar.png" alt="Avatar" style="width:100%"-->
            <div class="pic-post float-letf">
                <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( get_the_ID(),'full' );?></a>
                <div class="tittle-card float-letf">
                    <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                </div>
            </div>

            <div class="hastag float-letf">
                <?php foreach (wp_get_post_tags( get_the_ID()) as $index => $value){?>
                    <a href="#"><span><?php echo $value->name?></span></a>
                <?php } ?>
            </div>
        </div>
        <?php $i++;?>

    <?php endwhile; ?>

    <!--?php start_the_posts_navigation(); ?-->
</section>

