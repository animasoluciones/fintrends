<?php
/**
 * Created by PhpStorm.
 * User: yus
 * Date: 18/10/17
 * Time: 12:23
 */
?>
<?php $query = new WP_Query( array( 'cat' => 24 ) ); $i=1; ?>
<section class="flex-container flags">
    <div class="col-xs-12 col-sm-2 col-md-3">
        <h2 class="uppercase">Internacional</h2>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-9 display-flex">
        <ul class="ul-flags pd-0">
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/de.png"></a> </li>
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/uk.png"></a> </li>
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/it.png"></a> </li>
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/fr.png"></a> </li>
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/us.png"></a> </li>
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/ch.png"></a> </li>
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/jp.png"></a> </li>
            <li><a href="#"><img class="country" src="<?php echo get_theme_root_uri()?>/start/img/ue.png"></a> </li>
        </ul>
    </div>
</section>
<section class="flex-container mt-5<?php echo $query->have_posts()==true ?:'oculto';?>">
    <?php $post = $query->get_posts()[0];?>
    <?php $last = ($query->get_posts()[5]!=null) ? $query->get_posts()[5]:''?>

    <div class="col-xs-12 col-sm-4 col-md-4 pd-0 pt-5 first-int">
        <div class="flex-item-4 card">
            <!--img src="img_avatar.png" alt="Avatar" style="width:100%"-->
            <div data-link="<?php echo get_post_permalink($post->ID); ?>" class="pic-post float-letf new-tab" style="background-image:url( <?php echo wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array('200','200'), true )[0];?>);background-repeat: no-repeat;background-size: cover;">
                <div class="tittle-card float-letf">
                    <a href="<?php echo get_post_permalink($post->ID); ?>"><h2><?php echo get_the_title($post->ID); ?></h2></a>
                </div>
            </div>

            <div class="hastag float-letf">
                <?php foreach (wp_get_post_tags( $post->ID) as $index => $value){?>
                    <a href="#"><span><?php echo $value->name?></span></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 pt-2 middle-int">
        <?php while ( $query->have_posts()) : $query->the_post(); ?>

            <?php if($i>1 and $i<6):?>
            <div class="col-xs-12 col-sm-12 col-md-12 pb-2">
                    <div class="col-xs-12 col-sm-5 col-md-7">
                            <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( get_the_ID(),'full' );?></a>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-5 pd-0 overflow-hidden">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <div class="hastag float-letf pd-0">
                            <?php foreach (wp_get_post_tags( get_the_ID()) as $index => $value){?>
                                <a href="#"><span><?php echo $value->name?></span></a>
                            <?php } ?>
                        </div>
                    </div>
            </div>
                <?php endif;?>
            <?php $i++;?>

        <?php endwhile; ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 pd-0 pt-5 first-int">
        <div class="flex-item-4 card actors">
            <!--img src="img_avatar.png" alt="Avatar" style="width:100%"-->
            <!--div class="pic-post float-letf" style="background-image:url( <?php echo wp_get_attachment_image_src(get_post_thumbnail_id($last->ID), array('200','200'), true )[0];?>);background-repeat: no-repeat;background-size: cover;"-->
                <div class="tittle-card float-letf">
                    <!--a href="<?php echo get_post_permalink($last->ID); ?>"><h2><?php echo get_the_title($last->ID); ?></h2></a-->
                    <p class="uppercase">Actores</p>
                    <p class="uppercase">principales</p>
                </div>
                <div class="pic1">
                    <a href="<?php echo home_url('/')?>"><img class="img-responsive float-letf" src="<?php echo get_theme_root_uri()?>/start/img/wen.png" width="100"></a>
                    <a href="<?php echo home_url('/')?>"><img class="img-responsive float-right" src="<?php echo get_theme_root_uri()?>/start/img/fv.png" width="100"></a>
                </div>
                <div class="pic2">
                    <a href="<?php echo home_url('/')?>"><img class="img-responsive" src="<?php echo get_theme_root_uri()?>/start/img/hou.png" width="100"></a>
               </div>
                <div class="pic3">
                    <a href="<?php echo home_url('/')?>"><img class="img-responsive float-letf" src="<?php echo get_theme_root_uri()?>/start/img/len.png" width="100"></a>
                        <a href="<?php echo home_url('/')?>"><img class="img-responsive float-right" src="<?php echo get_theme_root_uri()?>/start/img/face.png" width="100"></a>
                </div>

            </div>
        </div>
    </div>
</section>