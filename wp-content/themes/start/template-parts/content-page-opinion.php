<?php
/**
 * Created by PhpStorm.
 * User: yusmel
 * Date: 28/11/17
 * Time: 13:16
 */
?>
<?php $query = new WP_Query( array( 'cat' => 36 ) ); $i=1; ?>
<div class="container">
    <section class="flex-container section-featured opinion <?php echo $query->have_posts()==true ?:'oculto';?>">
        <?php while ( $query->have_posts()) : $query->the_post(); ?>
            <div class="flex-item-4 card card-padding">
                <!--img src="img_avatar.png" alt="Avatar" style="width:100%"-->
                <div class="pic-post float-letf">
                    <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail( get_the_ID(),'full' );?></a>
                    <div class="tittle-card float-letf">
                        <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                    </div>
                </div>

                <div class="hastag float-letf">
                    <?php foreach (wp_get_post_tags( get_the_ID()) as $index => $value){?>
                        <a href="#"><span><?php echo $value->name?></span></a>
                    <?php } ?>
                </div>
            </div>
            <?php $i++;?>

        <?php endwhile; ?>

        <!--?php start_the_posts_navigation(); ?-->
    </section>
</div>

