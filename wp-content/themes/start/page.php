<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package start
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main col-md-12 pd-0" role="main">

			<?php while ( have_posts() ) : the_post(); ?>


                <?php if(is_page('ultimo')):?>
                    <?php get_template_part( 'template-parts/content', 'page-last' ); ?>
                <?php elseif(is_page('opinion')):?>
                    <?php get_template_part( 'template-parts/content', 'page-opinion' ); ?>
                <?php elseif(is_page('tendencia')):?>
                    <?php get_template_part( 'template-parts/content', 'page-tendencia' ); ?>
                <?php elseif(is_page('regulacion')):?>
                    <?php get_template_part( 'template-parts/content', 'page-regulacion' ); ?>
                <?php elseif(is_page('internacional')):?>
                    <?php get_template_part( 'template-parts/content', 'page-international' ); ?>
                <?php else:?>
                    <?php get_template_part( 'template-parts/content', 'page' ); ?>
                    <?php
                    // If comments are open or we have at least one comment, load up the comment template
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;
                    ?>

				 <?php endif;?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<!--?php get_sidebar(); ?-->
<?php get_footer(); ?>
