<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package start
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container mt-3">
			<div class="site-info">
				<a class="float-letf" href="<?php echo esc_url( __( home_url('/')) ); ?>"><img class="img-responsive logo" src="<?php echo get_theme_root_uri()?>/start/img/logofooter.png" width="100"></a>
                <ul class=" news-letter float-left pd-0">
                    <li><a class="open-nav" href="#">suscríbete a nuestra newsletter</a></li>
                    <li><a href="#">¿Quieres escribir en fintrends?</a></li>
                </ul>
                <ul class="network float-right pd-0">
                    <li class="pb-1"><a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/tf.png" width="40"></a> </li>
                    <li class="pb-1"><a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/lf.png" width="40"></a> </li>
                </ul>
                <ul class="footer-link float-right">
                    <li class="pb-1"><a class="uppercase" href="#">Contacto</a> </li>
                    <li class="pb-1"><a class="uppercase" href="#">Aviso legal</a> </li>
                    <li class="pb-1"><a class="uppercase" href="#">Equipo</a> </li>
                </ul>
			</div><!-- .site-info -->
		</div><!-- .container -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>
<!-- The overlay -->
<div id="myNav" class="overlay">

    <!-- Button to close the overlay navigation -->


    <!-- Overlay content -->
    <div class="overlay-content">
        <div class="item-flex">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <a href="javascript:void(0)" class="closebtn"><img class="img-responsive float-letf" src="<?php echo get_theme_root_uri()?>/start/img/close.png" width="25"></a>
                <h1 class="text-left uppercase pt-5">suscríbete a nuestra</h1>
                <h1 class="text-left uppercase news">newsletter</h1>
                <p class="text-left">y mantente informado de todas las novedades de fintrends</p>
            </div>
            <div class=" hid-this col-xs-12 col-sm-4 col-md-4 backpic">

            </div>
        </div>
    </div>

</div>
</body>
</html>
