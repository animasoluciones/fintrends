<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package start
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'start' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<div class="site-branding">
				<span class="site-title fecha capitalize"><?php echo '<span class="bold" style="color:#517a79">'.date('d').'</span> '.date_i18n('F Y');  ?></span>
				<!--h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1-->
				<!--h2 class="site-description"><?php bloginfo( 'description' ); ?></h2-->
                <?php echo get_search_form( $echo = true ); ?>
			</div><!-- .site-branding -->
            <div class="flex-container submenu-header mt-7 mb-6">
                <div class="col-xs-12 col-sm-10 col-md-10 titulo-header">

                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 pd-0 ft">
                    <a href="<?php echo home_url('/')?>"><img class="img-responsive logo mt-3 ml-3" src="<?php echo get_theme_root_uri()?>/start/img/logo.png" width="100"></a>
                </div>
            </div>

			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'start' ); ?></button>
                <a class="inicio" href="<?php echo home_url('/')?>"><img src="<?php echo get_theme_root_uri()?>/start/img/home.png" width="40"> </a>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav><!-- #site-navigation -->
		</div><!-- .container -->
        <ul class="redes">
            <li><a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/twitter.png" width="40"></a> </li>
            <li><a href="#"><img src="<?php echo get_theme_root_uri()?>/start/img/linkedin.png" width="40"></a> </li>
        </ul>
	</header><!-- #masthead -->

    <?php if (is_single() || is_page()):?>
	    <div id="content" class="site-content container-fluid pd-0">
    <?php else:?>
            <div id="content" class="site-content container">
    <?php endif;?>
