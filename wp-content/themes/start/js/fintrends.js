$(function() {
    console.log($(document).width());
    fn={
        initialization:function () {
            $('.new-tab').css('cursor','pointer').click(function () {
                location.href=$(this).attr('data-link');
            })
        },
        menu:function () {
            $.each($('.menu-item a'),function (event) {
                // cambiar cuando este en el servidor

                if(location.pathname.substring(1,location.pathname.length-1)==$(this).text().toLowerCase()){
                    $('.menu-item a.active').removeClass('active');
                    $(this).addClass('active');
                    return false;
                }else if($(this).text().toLowerCase()=='regulación' && location.pathname.substring(1,location.pathname.length-1)=='regulacion'){
                    $('.menu-item a.active').removeClass('active');
                    $(this).addClass('active');
                    return false;
                }else if($(this).text().toLowerCase()=='opinión' && location.pathname.substring(1,location.pathname.length-1)=='opinion'){
                    $('.menu-item a.active').removeClass('active');
                    $(this).addClass('active');
                    return false;
                }else if($(this).text().toLowerCase()=='lo último' && location.pathname.substring(1,location.pathname.length-1)=='ultimo'){
                    $('.menu-item a.active').removeClass('active');
                    $(this).addClass('active');
                    return false;
                }else if($(this).text().toLowerCase()=='eventos' && location.href.substring(location.href.length-6,location.href.length)=='events'){
                    $('.menu-item a.active').removeClass('active');
                    $(this).addClass('active');
                    return false;
                }

            })
        },
        carousel:function (event) {
            $('.next').click(function (e) {
                e.preventDefault();
                var all=$('.opinion .mostrar');

                if($(all[3]).next().hasClass('oculto')){
                    $(all[0]).removeClass('mostrar').addClass('oculto');
                    $(all[1]).addClass('f-0');
                    $(all[3]).next().removeClass('oculto').addClass('mostrar');
                }

            });

            $('.prev').click(function (e) {
                e.preventDefault();
                var all=$('.opinion .mostrar');


                if($(all[0]).prev().hasClass('oculto')){
                    $(all[0]).prev().removeClass('oculto').addClass('mostrar f-0');
                    $(all[0]).removeClass('f-0');
                    $(all[3]).removeClass('mostrar').addClass('oculto');
                }
            });
        },
        modal:function () {
            $('.open-nav').click(function () {
                document.getElementById("myNav").style.width = "100%";
            });

            $('.closebtn').click(function () {
                document.getElementById("myNav").style.width = "0%";
            })

        },
        clanedario:function () {
            var meses=['ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIERMBRE','DICIEMBRE'];
            var days=new Date(new Date().getFullYear(), new Date().getMonth()+1, 0).getDate();

            //createCalendar(new Date());

           /* $('.img-calen').css('cursor','pointer').click(function () {
                $('.tab-eventos').show();
            });
            $(window).click(function() {
                $('.tab-eventos').hide();
            });

            $('.img-calen').click(function(event){
                event.stopPropagation();
            });*/

            $('.sig').click(function () {
                var m = $('.meses').attr('data-mes');
                var dt = new Date(new Date().getFullYear(), (parseInt(m)));
                $('.meses').text(meses[dt.getMonth()]);
                jQuery.getJSON("/wp-admin/admin-ajax.php",
                    {
                        action: 'get_treatments', post_type: 'tribe_events', yy: dt.getFullYear(),mm:dt.getMonth(),
                    },
                    function (data)
                    {
                        $.each(data,function (index,value) {
                            console.log(value);
                            $($('.p-dias')[index]).html(new Date(value.EventStartDate).getDate()+'-'+new Date(value.EventEndDate).getDate()+'<br><span class="span-m">'+meses[new Date(value.EventEndDate).getMonth()].substring(0,3)+'</span>');
                            //$($('.p-dias span')[index]).text();
                            //$($('table td')[new Date(value.EventStartDate).getDate()]).css('background','red');
                        })

                    }
                    )

                $('.meses').attr('data-mes',parseInt(m)+1);

            });


            function createCalendar(date) {
                $('.meses').html('<b>'+meses[date.getMonth()]+"</b>-"+meses[date.getMonth()+1]).attr('data-mes',date.getMonth()).css({'color':'##000','font-family':'Roboto-Bold','margin-bottom':'3px'});
                var init =new Date(date.getFullYear(), date.getMonth(), 1).getDay();
                $.each($('.table th'),function (index) {
                    if(index==init){
                        for(var i=0;i<(days+init);i++){
                            if(i%7==0){
                                var tr= $('<tr/>').attr('data-id',i).appendTo($('table tbody'));
                            }

                            if(i>=init){

                                var td=$('<td/>').attr('data-day',(i-init+1)).addClass("td "+(i-init+1)+"").appendTo(tr);
                                var t=$('<i/>').text(i-init+1).appendTo(td);
                            }else{
                                var td=$('<td/>').addClass('td').appendTo(tr);
                            }
                        }
                        return;
                    }
                })
            }


        }
    }

    fn.menu();
    fn.carousel();
    fn.modal();
    fn.clanedario();
    fn.initialization();
})