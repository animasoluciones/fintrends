��    @        Y         �  
   �  
   �     �  	   �  ;   �     �     �  
   �                    '  8   <     u     }  
   �  _   �  W   �     E     L  	   \     f     {     �     �     �     �  %   �     �     �     �               $     1  F   G     �     �     �     �  \   �     &	     ,	     4	     @	  )   U	     	  T   �	  !   �	     �	      
     6
     H
      _
     �
     �
  #   �
      �
           %  "   F      i     �    �     �  
   �     �     �  7   �            	      
   *     5     D     ^  5   t     �     �     �  d   �  S   0  
   �     �     �     �     �     �     �          "  &   4  
   [  	   f     p     �     �     �     �  K   �          =     K     a  ~   q     �     �            /   .     ^  P   g     �     �     �     �     �     �             	     	              (     .     6     >                   =                  (          5   +   "           @                            3   9          -   /      :      2         .   1   ,                               <              )   ;          *       
   %               4   6   !                        ?   8   0              7   '   $         #   >   &   	    % Comments %1$s: %2$s ,  1 Comment A minimal starter theme based on Underscores and Bootstrap. Archives Archives: %s Author: %s Boxed Category: %s Comment navigation Comments are closed. Continue reading %s <span class="meta-nav">&rarr;</span> Day: %s Edit Full Width It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Layout Leave a comment Month: %s Most Used Categories Newer Comments Newer posts Nothing Found Older Comments Older posts Oops! That page can&rsquo;t be found. Page %s Pages: Post navigation Posted in %1$s Posts navigation Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Sidebar Site Layout Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Start Tag: %s Tagged %1$s Theme: %1$s by %2$s. Try looking in the monthly archives. %1$s Year: %s comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; daily archives date formatF j, Y http://wordpress.org/ monthly archives date formatF Y post authorby %s post datePosted on %s post format archive titleAsides post format archive titleAudio post format archive titleChats post format archive titleGalleries post format archive titleImages post format archive titleLinks post format archive titleQuotes post format archive titleStatuses post format archive titleVideos yearly archives date formatY PO-Revision-Date: 2017-01-29 03:33:58+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Themes - Start
 % comentarios %1$s: %2$s ,  1 comentario Un tema base mínimo basado en Underscores y Bootstrap. Archivo Archivos: %s Autor: %s Encuadrada Categoría: %s Comentario de navegación Comentarios cerrados. Sigue leyendo %s <span class="meta-nav">&rarr;</span> Día: %s Editor Anchura Completa Parece que no se encontró nada aquí. ¿Quieres probar uno de los enlaces de abajo o una búsqueda? Parece que no encontramos lo que estás buscando. Puede que una búsqueda te ayude. Estructura Deje su comentario Mes: %s Categorías más utilizadas Comentarios más recientes Artículos más recientes Nada encontrado Comentarios anteriores Entradas antiguas ¡Vaya! Está página no se encuentra. Página %s Páginas: Navegación de la entrada Publicada en %1$s Navegación de entradas Menú Primario Creado con %s ¿Listo para publicar tu primera entrada? <a href="%1$s">Empieza aquí</a>. Resultados de búsqueda por: %s Barra lateral Disposición de Sitio Ir al contenido Lo sentimos, pero no encaja nada con los términos de la búsqueda. Inténtalo de nuevo con algunas palabras clave diferentes. Empezar Etiqueta: %s Etiquetado con %1$s Tema: %1$s por %2$s. Trata de buscar en los archivos mensuales. %1$s Año: %s Un comentario sobre &ldquo;%2$s&rdquo; %1$s comentarios sobre &ldquo;%2$s&rdquo; j F, Y http://es.wordpress.org/ M A por %s Publicada en %s Minientradas Audio Chats Galerías Imágenes Enlaces Citas Estatus Vídeos A 