<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'fintrends');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'yusmel');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'wG}J`$X*:5V$,LN2d>~V//KyZ9SBf%%!iLN;w_AZ([ YMz0ZMH5JY}mUQo`$og(}');
define('SECURE_AUTH_KEY', '7_+Et3fv9_b]e;NJWTXt=~Oijj+A+nD1IS}/YJiE_Tk*]gw?+APG8QvB# EF;$EX');
define('LOGGED_IN_KEY', '*Lzd|GvNpY%5HO#)`K1{yTJLkU6P~*>G#;S_ZG2av_]LV64|+:,@N;)6$E-5]l.h');
define('NONCE_KEY', 'B5^U~?^`4-XfcmwhA2$MPRTp_?@VON?Wb+:KanFec=X:sfK(b1aR2a]>yo*0F_tp');
define('AUTH_SALT', 'qxyzL,B|xd# VC#A67140f.vpB):% <rq8)GJ:fTa4B`$ag;#0Igk-tL=)ZxL(+-');
define('SECURE_AUTH_SALT', '_#@KSV,d(=:5#^v:4t{6F=Y3W``q.~T|<qNnxwny?}-xEso]r45w53%#8.Df/nTW');
define('LOGGED_IN_SALT', 'n]awZ:8S wo,}-fJ)hfmz?EEn))aS,$%6-8DX^28pYn/v9u%oDVJaluEt<3jMM?R');
define('NONCE_SALT', 'To+0F~zX Q-/KV2>_1:cIW)x6!*?hM8Q@kvKyu}i>%v~V.Kk]osg`N=L$E/{};6]');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'ft_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

